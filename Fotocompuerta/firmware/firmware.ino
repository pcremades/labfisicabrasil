/* Copyright 2018 Pablo Cremades
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**************************************************************************************
* Autor: Pablo Cremades
* Fecha: 20/08/2018
* e-mail: pcremades@fcen.uncu.edu.ar
* Descripción: firmware para Arduino para la fotocompuerta (https://gitlab.com/pcremades/Taller_Arduino/tree/master/Fotocompuerta).
*  Este firmware esta diseñado para ser compatible con el protocolo de comunicacion del sistema de adquisicion de datos
*  de INGKA (https://es-la.facebook.com/ingka.educacion/).
*
*/

// Pin 2 y 3 son entradas para las fotocompuertas. Utilizan las interrupciones externas.
// Pines 11 y 12 se utilizan para alimentar los leds de las fotocompuertas en caso de querer encenderlos y apagarlos.
int IRD1=2, IRD2 = 3, IRE1=12, IRE2=11;

void setup() {
  Serial.begin(115200);
//Usamos pullups externos de 330kOhm para mejorar la sensibilidad.
  pinMode(IRD1, INPUT);
  pinMode(IRD2, INPUT);
//  pinMode(IRD1, INPUT_PULLUP); 
//  pinMode(IRD2, INPUT_PULLUP);
  pinMode(IRE2, OUTPUT);
  pinMode(IRE1, OUTPUT); 
  digitalWrite(IRE1, HIGH); 
  digitalWrite(IRE2, HIGH);
  attachInterrupt(INT0, IRD1int, CHANGE);
  attachInterrupt(INT1, IRD2int, CHANGE);
}

void loop() {
  
}

//ISR para la fotocompuerta 1. Imprime un mensaje compatible con el protocolo INGKA.
void IRD1int ()
{
    Serial.print("2\t");
    Serial.print(micros());
    Serial.print('\t');
    Serial.println(digitalRead(2));
}

//ISR para la fotocompuerta 2. Imprime un mensaje compatible con el protocolo INGKA.
void IRD2int ()
{
    Serial.print("3\t");
    Serial.print(micros());
    Serial.print('\t');
    Serial.println(digitalRead(3));
}
