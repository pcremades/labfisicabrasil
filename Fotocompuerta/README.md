# Fotocompuerta
Esta es una fotocompuerta simple y de bajo costo para experimentos de física.
Se puede conectar a cualquier microcontrolador, como por ejemplo Arduino, para medir tiempos con
mucha precisión.

![Fotocompuerta](https://gitlab.com/pcremades/Taller_Arduino/raw/master/Fotocompuerta/Img/fotogate%20cerrada.jpg)

## Construcción

### Materiales
- LED infrarrojo.
- Fotodiodo o fototransistor infrarrojo.
- Transistor bipolar de señal BC548, 2N2222 o cualquiera equivalente.
- Resistencia de 220 Ohm y resistencia de 10 kOhm.

*Nota: En Mendoza se puede conseguir un juego de [LED y fotodiodo](http://tienda.ityt.com.ar/mercadolibre/6790-juego-transmisor-receptor-infrarrojo-5mm-ir-940nm-apareados-itytarg.html?search_query=infrarrojo&results=79) apropiados para esto.

### Diagrama de conexión
![Sketch](https://tecnologias.libres.cc/andrea.rincon/laboratorio_de_fisica/raw/master/Fotocompuerta/Img/Sketch.png)

Puede descargar el esquemático diseñado en Fritzing [aquí](https://tecnologias.libres.cc/andrea.rincon/laboratorio_de_fisica/raw/master/Fotocompuerta/Sketch.fzz).
Este proyecto está basado en un [desarrollo previo](https://gitlab.com/pcremades/Taller_Arduino/tree/master/Fotocompuerta).

### Conexión con Arduino
La fotocompuerta se puede conectar a los pines 2 o 3 de Arduino
![Sketch](https://tecnologias.libres.cc/andrea.rincon/laboratorio_de_fisica/raw/master/Fotocompuerta/Img/ConnectArduino.png)

Descargue el [firmware](https://tecnologias.libres.cc/andrea.rincon/laboratorio_de_fisica/raw/master/Fotocompuerta/firmware/firmware.ino)

Puede encontrar una aplicación para hacer experimentos de caída libre [aquí](https://gitlab.com/pcremades/Taller_Arduino/tree/master/CaidaLibre).

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licencia Creative Commons Atribución-NoComercial 4.0 Internacional</a>.
